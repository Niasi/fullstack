class User < ApplicationRecord
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }

    def user_params
        params.require(:user).permit(:name, :lastname, :email, :phone, :password, :password_confirmation)
    end

    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end
end
