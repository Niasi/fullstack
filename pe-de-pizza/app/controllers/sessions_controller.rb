class SessionsController < ApplicationController
  
  def new
    @user = User.new
  end
  
  def create
    user = User.find_by(email: params[:session][:email.downcase])
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to root_path
    else
      @user = User.new
      flash.now[:notice] = 'Combinação de e-mail e senha inválida.'
      respond_to do |format|
      format.html {
        flash.now[:notice] = 'Combinação de e-mail e senha inválida.'
        render :new, notice: 'AWDWADA'
      }
      end
    end
  end
  
  def destroy
    log_out
    redirect_to root_url
  end

end
